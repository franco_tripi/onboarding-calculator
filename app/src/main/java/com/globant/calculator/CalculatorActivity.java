package com.globant.calculator;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.globant.calculator.mvp.model.CalculatorModel;
import com.globant.calculator.mvp.presenter.CalculatorPresenter;
import com.globant.calculator.mvp.view.CalculatorView;

public class CalculatorActivity extends AppCompatActivity {

    private CalculatorPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculator);
        presenter = new CalculatorPresenter(new CalculatorModel(), new CalculatorView(this));
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.registerRxBus();
    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.unregisterRxBus();
    }
}
