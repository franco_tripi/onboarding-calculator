package com.globant.calculator.util.bus.observers;

public abstract class EqualButtonBusObserver extends BusObserver<EqualButtonBusObserver.EqualButton> {

    public EqualButtonBusObserver() {
        super(EqualButtonBusObserver.EqualButton.class);
    }

    public static class EqualButton {
        //Do Nothing...
    }
}
