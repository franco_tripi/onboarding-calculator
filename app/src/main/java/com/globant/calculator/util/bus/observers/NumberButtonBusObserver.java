package com.globant.calculator.util.bus.observers;

import android.support.annotation.NonNull;

public abstract class NumberButtonBusObserver extends BusObserver<NumberButtonBusObserver.NumberButton> {

    public NumberButtonBusObserver() {
        super(NumberButton.class);
    }

    public static class NumberButton {

        private int digit;

        /**
         * Number Button Constructor
         *
         * @param digit integer from 0 to 9 that represents the button value
         */
        public NumberButton(@NonNull int digit) {
            this.digit = digit;
        }

        @NonNull
        public int getDigit() {
            return digit;
        }
    }
}
