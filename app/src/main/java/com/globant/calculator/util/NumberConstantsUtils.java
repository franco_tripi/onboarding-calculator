package com.globant.calculator.util;


public class NumberConstantsUtils {

    public static final int ZERO = 0;
    public static final int ONE = 1;
    public static final int TWO = 2;
    public static final int FIVE = 5;
    public static final int TEN = 10;

    public static final double DOUBLE_ZERO = 0D;
    public static final double DOUBLE_FOURTEEN = 14;
    public static final double DOUBLE_FIVE = 5.0;
    public static final double DOUBLE_NINE = 9.0;
    public static final double DOUBLE_NINETEEN = 19.0;
    public static final double DOUBLE_TWENTY_FIVE = 25.0;
    public static final double DOUBLE_FIFTY_TWO = 52.0;
}
