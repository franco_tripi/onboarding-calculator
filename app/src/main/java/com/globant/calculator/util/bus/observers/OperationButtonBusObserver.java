package com.globant.calculator.util.bus.observers;

import com.globant.calculator.mvp.model.CalculatorModel;

public abstract class OperationButtonBusObserver extends BusObserver<OperationButtonBusObserver.OperationButton> {

    public OperationButtonBusObserver() {
        super(OperationButtonBusObserver.OperationButton.class);
    }

    public static class OperationButton {

        private int operation;

        public OperationButton(@CalculatorModel.OperationMode int operation) {
            this.operation = operation;
        }

        public int getOperation() {
            return operation;
        }
    }
}
