package com.globant.calculator.util.bus.observers;

public abstract class CleanButtonBusObserver extends BusObserver<CleanButtonBusObserver.CleanButton> {

    public CleanButtonBusObserver() {
        super(CleanButton.class);
    }

    public static class CleanButton {
        //Do Nothing...
    }
}
