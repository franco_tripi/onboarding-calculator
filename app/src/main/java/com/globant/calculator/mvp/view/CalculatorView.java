package com.globant.calculator.mvp.view;

import android.app.Activity;
import android.widget.Button;
import android.widget.TextView;

import com.globant.calculator.R;
import com.globant.calculator.mvp.model.CalculatorModel;
import com.globant.calculator.util.StringUtils;
import com.globant.calculator.util.bus.RxBus;
import com.globant.calculator.util.bus.observers.CleanButtonBusObserver;
import com.globant.calculator.util.bus.observers.EqualButtonBusObserver;
import com.globant.calculator.util.bus.observers.NumberButtonBusObserver;
import com.globant.calculator.util.bus.observers.OperationButtonBusObserver;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.annotations.NonNull;

public class CalculatorView extends ActivityView {

    @BindView(R.id.text_display) TextView calculatorDisplay;

    public CalculatorView(Activity activity) {
        super(activity);
        ButterKnife.bind(this, activity);
    }

    /**
     * Adds a new value (digit or symbol) to the display
     *
     * @param value digit or symbol to show in display
     */
    public void addValueToDisplay(@NonNull String value) {
        calculatorDisplay.setText(calculatorDisplay.getText() + value);
    }

    public void cleanDisplay() {
        calculatorDisplay.setText(StringUtils.EMPTY_STRING);
    }

    @OnClick({R.id.button_zero, R.id.button_one, R.id.button_two, R.id.button_three, R.id.button_four, R.id.button_five,
            R.id.button_six, R.id.button_seven, R.id.button_eight, R.id.button_nine})
    public void numberButtonPressed(Button button) {
        RxBus.post(new NumberButtonBusObserver.NumberButton(Integer.parseInt(button.getText().toString())));
    }

    @OnClick(R.id.button_clean)
    public void cleanButtonPressed() {
        RxBus.post(new CleanButtonBusObserver.CleanButton());
    }

    @OnClick(R.id.button_addition)
    public void additionButtonPressed() {
        RxBus.post(new OperationButtonBusObserver.OperationButton(CalculatorModel.OperationMode.ADDITION));
    }

    @OnClick(R.id.button_subtraction)
    public void subtractionButtonPressed() {
        RxBus.post(new OperationButtonBusObserver.OperationButton(CalculatorModel.OperationMode.SUBTRACTION));
    }

    @OnClick(R.id.button_equal)
    public void equalButtonPressed() {
        RxBus.post(new EqualButtonBusObserver.EqualButton());
    }

    @OnClick(R.id.button_multiplication)
    public void multiplicationButtonPressed() {
        RxBus.post(new OperationButtonBusObserver.OperationButton(CalculatorModel.OperationMode.MULTIPLICATION));
    }
}
