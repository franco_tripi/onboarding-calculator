package com.globant.calculator.mvp.model;

import android.support.annotation.IntDef;
import android.support.annotation.NonNull;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import static com.globant.calculator.util.NumberConstantsUtils.DOUBLE_ZERO;

public class CalculatorModel {

    @OperationMode
    private int operation;

    private double operator1 = DOUBLE_ZERO;
    private double operator2 = DOUBLE_ZERO;

    private boolean isOperator1Setted = false;

    public int getOperation() {
        return operation;
    }

    /**
     * Set the operation to be resolve (Addition, Subtraction, Multiplication, Division)
     *
     * @param operation to be resolved
     */
    public void setOperation(@NonNull final int operation) {
        this.operation = operation;
    }

    @NonNull
    public double getOperator1() {
        return operator1;
    }

    public void setOperator1(@NonNull final double operator1) {
        this.operator1 = operator1;
        isOperator1Setted = true;
    }

    @NonNull
    public double getOperator2() {
        return operator2;
    }

    public void setOperator2(@NonNull final double operator2) {
        this.operator2 = operator2;
    }

    public boolean isOperator1Setted() {
        return isOperator1Setted;
    }

    public void reset() {
        operator1 = DOUBLE_ZERO;
        operator2 = DOUBLE_ZERO;
        operation = OperationMode.NONE;
        isOperator1Setted = false;
    }

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({OperationMode.NONE, OperationMode.ADDITION, OperationMode.SUBTRACTION, OperationMode.MULTIPLICATION})
    public @interface OperationMode {
        int NONE = 0;
        int ADDITION = 1;
        int SUBTRACTION = 2;
        int MULTIPLICATION = 3;
    }
}
