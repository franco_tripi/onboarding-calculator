package com.globant.calculator.mvp.presenter;

import android.app.Activity;
import android.support.annotation.NonNull;

import com.globant.calculator.mvp.model.CalculatorModel;
import com.globant.calculator.mvp.view.CalculatorView;
import com.globant.calculator.util.bus.RxBus;
import com.globant.calculator.util.bus.observers.CleanButtonBusObserver;
import com.globant.calculator.util.bus.observers.EqualButtonBusObserver;
import com.globant.calculator.util.bus.observers.NumberButtonBusObserver;
import com.globant.calculator.util.bus.observers.OperationButtonBusObserver;

import static com.globant.calculator.util.NumberConstantsUtils.DOUBLE_ZERO;
import static com.globant.calculator.util.NumberConstantsUtils.ONE;
import static com.globant.calculator.util.NumberConstantsUtils.TEN;
import static com.globant.calculator.util.NumberConstantsUtils.ZERO;

public class CalculatorPresenter {

    public static final String ADDITION_SYMBOL = "+";
    public static final String SUBTRACTION_SYMBOL = "-";
    public static final String MULTIPLICATION_SYMBOL = "*";

    private double number;

    private CalculatorView view;
    private CalculatorModel model;

    /**
     * Calculator Presenter Constructor
     *
     * @param model Calculator model
     * @param view  Calculator view
     */
    public CalculatorPresenter(CalculatorModel model, CalculatorView view) {
        this.view = view;
        this.model = model;
    }

    /**
     * Builds the number by typed digits and shows it on calculator display
     *
     * @param digit selected number in the calculator
     */
    public void onNumberButtonPressed(@NonNull int digit) {
        this.number = this.number * TEN + digit;
        view.addValueToDisplay(String.valueOf(digit));
    }

    /**
     * Reset the model values and the calculator display
     */
    public void onCleanButtonPressed() {
        resetNumber();
        model.reset();
        view.cleanDisplay();
    }

    /**
     * Communicate to the model which operation is going to be performed
     * and shows the appropriate symbol in the calculator display
     */
    public void onAdditionButtonPressed() {
        addOperator();
        model.setOperation(CalculatorModel.OperationMode.ADDITION);
        view.addValueToDisplay(ADDITION_SYMBOL);
    }

    public void onSubtractionButtonPressed() {
        addOperator();
        model.setOperation(CalculatorModel.OperationMode.SUBTRACTION);
        view.addValueToDisplay(SUBTRACTION_SYMBOL);
    }

    public void onMultiplicationButtonPressed() {
        addOperator();
        model.setOperation(CalculatorModel.OperationMode.MULTIPLICATION);
        view.addValueToDisplay(MULTIPLICATION_SYMBOL);
    }

    /**
     * Clean the calculator display and shows the operation result
     */
    public void onEqualButtonPressed() {
        addOperator();
        view.cleanDisplay();
        view.addValueToDisplay(String.valueOf(model.getOperator1()));
    }

    /**
     * Assigns the number entered to the correct operator. In case to be more than two operators,
     * operator1 stores the partial result.
     */
    public void addOperator() {
        if (!model.isOperator1Setted()) {
            if (model.getOperation() == CalculatorModel.OperationMode.SUBTRACTION) {
                this.number = this.number * (-ONE);
            }
            model.setOperator1(this.number);
        } else {
            model.setOperator2(this.number);
            model.setOperator1(getResult());
            model.setOperator2(DOUBLE_ZERO);
        }
        resetNumber();
    }

    /**
     * Performs the mathematical operation entered between operators
     *
     * @return result mathematical operation result
     */
    @NonNull
    public double getResult() {
        double result = DOUBLE_ZERO;
        switch (model.getOperation()) {
            case CalculatorModel.OperationMode.ADDITION:
                result = model.getOperator1() + model.getOperator2();
                break;
            case CalculatorModel.OperationMode.SUBTRACTION:
                result = model.getOperator1() - model.getOperator2();
                break;
            case CalculatorModel.OperationMode.MULTIPLICATION:
                result = model.getOperator1() * model.getOperator2();
                break;
            default:
                break;
        }
        return result;
    }

    private void resetNumber() {
        this.number = ZERO;
    }

    public final void registerRxBus() {
        final Activity activity = view.getActivity();
        if (activity != null) {

            //The presenter subscribes to a calculator number pad event
            RxBus.subscribe(activity, new NumberButtonBusObserver() {
                @Override
                public void onEvent(NumberButtonBusObserver.NumberButton value) {
                    onNumberButtonPressed(value.getDigit());
                }
            });

            //The presenter subscribes to an operation button event
            RxBus.subscribe(activity, new OperationButtonBusObserver() {
                @Override
                public void onEvent(OperationButton value) {
                    switch (value.getOperation()) {
                        case CalculatorModel.OperationMode.ADDITION:
                            onAdditionButtonPressed();
                            break;
                        case CalculatorModel.OperationMode.SUBTRACTION:
                            onSubtractionButtonPressed();
                            break;
                        case CalculatorModel.OperationMode.MULTIPLICATION:
                            onMultiplicationButtonPressed();
                            break;
                        default:
                            break;
                    }
                }
            });

            //The presenter subscribes to an equal button event
            RxBus.subscribe(activity, new EqualButtonBusObserver() {
                @Override
                public void onEvent(EqualButton value) {
                    onEqualButtonPressed();
                }
            });

            //The Presenter subscribes to a clean display button event
            RxBus.subscribe(activity, new CleanButtonBusObserver() {
                @Override
                public void onEvent(CleanButton value) {
                    onCleanButtonPressed();
                }
            });
        }
    }

    public final void unregisterRxBus() {
        final Activity activity = view.getActivity();
        if (activity != null) {
            RxBus.clear(activity);
        }
    }
}
