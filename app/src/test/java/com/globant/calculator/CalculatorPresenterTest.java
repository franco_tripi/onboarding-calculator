package com.globant.calculator;

import com.globant.calculator.mvp.model.CalculatorModel;
import com.globant.calculator.mvp.presenter.CalculatorPresenter;
import com.globant.calculator.mvp.view.CalculatorView;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static com.globant.calculator.util.NumberConstantsUtils.DOUBLE_FIFTY_TWO;
import static com.globant.calculator.util.NumberConstantsUtils.DOUBLE_FIVE;
import static com.globant.calculator.util.NumberConstantsUtils.DOUBLE_FOURTEEN;
import static com.globant.calculator.util.NumberConstantsUtils.DOUBLE_NINE;
import static com.globant.calculator.util.NumberConstantsUtils.DOUBLE_NINETEEN;
import static com.globant.calculator.util.NumberConstantsUtils.DOUBLE_TWENTY_FIVE;
import static com.globant.calculator.util.NumberConstantsUtils.DOUBLE_ZERO;
import static com.globant.calculator.util.NumberConstantsUtils.FIVE;
import static com.globant.calculator.util.NumberConstantsUtils.TWO;
import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

public class CalculatorPresenterTest {

    @Mock CalculatorModel model;
    @Mock CalculatorView view;
    private CalculatorPresenter presenter;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        presenter = new CalculatorPresenter(model, view);
    }

    @Test
    public void onNumberButtonPressedShowTheDigitOnCalculatorDisplay() {
        presenter.onNumberButtonPressed(FIVE);
        verify(view).addValueToDisplay(String.valueOf(FIVE));
    }

    @Test
    public void onCleanButtonPressedResetModelAndCleanCalculatorDisplay() {
        presenter.onCleanButtonPressed();
        verify(model).reset();

        final int operationMode = model.getOperation();
        final double operator1 = model.getOperator1();
        final double operator2 = model.getOperator2();

        assertEquals(CalculatorModel.OperationMode.NONE, operationMode);
        assertEquals(DOUBLE_ZERO, operator1);
        assertEquals(DOUBLE_ZERO, operator2);

        verify(view).cleanDisplay();
        verifyNoMoreInteractions(view);
    }

    @Test
    public void onEqualButtonPressedReturnsTheValueStoredInOperator1() {
        when(model.getOperator1()).thenReturn(DOUBLE_FOURTEEN);

        presenter.onEqualButtonPressed();

        verify(view).cleanDisplay();
        verify(view).addValueToDisplay(String.valueOf(DOUBLE_FOURTEEN));
    }

    @Test
    public void getResultMethodWhenOperationIsSumReturnsTheSumOfTheValues() {
        when(model.getOperator1()).thenReturn(DOUBLE_FIVE);
        when(model.getOperation()).thenReturn(CalculatorModel.OperationMode.ADDITION);
        when(model.getOperator2()).thenReturn(DOUBLE_FOURTEEN);

        final double result = presenter.getResult();

        assertEquals(DOUBLE_NINETEEN, result);
    }

    @Test
    public void getResultMethodWhenOperationIsSumAndFirstOperatorIsNegativeReturnsTheSumOfTheValues() {
        when(model.getOperator1()).thenReturn(-DOUBLE_FIVE);
        when(model.getOperation()).thenReturn(CalculatorModel.OperationMode.ADDITION);
        when(model.getOperator2()).thenReturn(DOUBLE_FOURTEEN);

        final double result = presenter.getResult();

        assertEquals(DOUBLE_NINE, result);
    }

    @Test
    public void getResultMethodWhenOperationIsSubtractionReturnsTheSubtractionOfTheValues() {
        when(model.getOperator1()).thenReturn(DOUBLE_FIVE);
        when(model.getOperation()).thenReturn(CalculatorModel.OperationMode.SUBTRACTION);
        when(model.getOperator2()).thenReturn(DOUBLE_FOURTEEN);

        final double result = presenter.getResult();

        assertEquals(-DOUBLE_NINE, result);
    }

    @Test
    public void getResultMethodWhenOperationIsSubtractionAndFirstOperatorIsNegativeReturnsTheSubtractionOfTheValues() {
        when(model.getOperator1()).thenReturn(-DOUBLE_FIVE);
        when(model.getOperation()).thenReturn(CalculatorModel.OperationMode.SUBTRACTION);
        when(model.getOperator2()).thenReturn(DOUBLE_FOURTEEN);

        final double result = presenter.getResult();

        assertEquals(-DOUBLE_NINETEEN, result);
    }

    @Test
    public void getResultMethodWhenOperationIsMulReturnsTheMulOfTheValues() {
        when(model.getOperator1()).thenReturn(DOUBLE_FIVE);
        when(model.getOperation()).thenReturn(CalculatorModel.OperationMode.MULTIPLICATION);
        when(model.getOperator2()).thenReturn(DOUBLE_FIVE);

        final double result = presenter.getResult();

        assertEquals(DOUBLE_TWENTY_FIVE, result);
    }

    @Test
    public void getResultMethodWhenOperationIsMulAndFirstOperatorIsNegativeReturnsTheMulOfTheValues() {
        when(model.getOperator1()).thenReturn(-DOUBLE_FIVE);
        when(model.getOperation()).thenReturn(CalculatorModel.OperationMode.MULTIPLICATION);
        when(model.getOperator2()).thenReturn(DOUBLE_FIVE);

        final double result = presenter.getResult();

        assertEquals(-DOUBLE_TWENTY_FIVE, result);
    }

    @Test
    public void getResultMethodWhenOperationIsNoneReturnsTheSumOfTheValues() {
        when(model.getOperator1()).thenReturn(DOUBLE_FIVE);
        when(model.getOperation()).thenReturn(CalculatorModel.OperationMode.NONE);
        when(model.getOperator2()).thenReturn(DOUBLE_FOURTEEN);

        final double result = presenter.getResult();

        assertEquals(DOUBLE_ZERO, result);
    }

    @Test
    public void addOperatorWhenThereIsNotOperatorSetted() {
        when(model.isOperator1Setted()).thenReturn(false);

        presenter.onNumberButtonPressed(TWO);
        presenter.onNumberButtonPressed(FIVE);
        presenter.addOperator();

        verify(model).setOperator1(DOUBLE_TWENTY_FIVE);

        final double operator2 = model.getOperator2();
        assertEquals(DOUBLE_ZERO, operator2);
    }

    @Test
    public void addOperatorWhenThereIsOneOperatorSetted() {
        when(model.isOperator1Setted()).thenReturn(true);

        presenter.onNumberButtonPressed(FIVE);
        presenter.onNumberButtonPressed(TWO);
        presenter.addOperator();

        verify(model).setOperator2(DOUBLE_FIFTY_TWO);
    }

    @Test
    public void addOperatorWhenThereAreBothOperatorsSetted() {
        when(model.isOperator1Setted()).thenReturn(true);
        when(model.getOperator1()).thenReturn(DOUBLE_TWENTY_FIVE);
        when(model.getOperator2()).thenReturn(DOUBLE_FIFTY_TWO);
        when(model.getOperation()).thenReturn(CalculatorModel.OperationMode.ADDITION);

        presenter.onNumberButtonPressed(TWO);
        presenter.onNumberButtonPressed(FIVE);
        presenter.addOperator();

        verify(model).setOperator1(presenter.getResult());
        verify(model).setOperator2(DOUBLE_TWENTY_FIVE);
    }
}
